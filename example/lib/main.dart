import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:flutter_thermometer/thermometer.dart';
import 'custom_scale_provider.dart';

void main() => runApp(ThermometerDemoApp());

class ThermometerDemoApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ThermometerDemoWrapper(),
    );
  }
}

class ThermometerDemoWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) => ThermometerDemo();
}

class ThermometerDemo extends StatefulWidget {
  @override
  _ThermometerDemoState createState() => _ThermometerDemoState();
}

enum TempLabels { CELCIUS, FARENHEIT, CUSTOM }

class _ThermometerDemoState extends State<ThermometerDemo>
    with TickerProviderStateMixin {
  bool fullscreen = false;
  double thermoWidth = 150;
  double thermoHeight = 150;
  double minValue = -10;
  double maxValue = 70;
  double value = 30;
  double radius = 30.0;
  double barWidth = 30.0;
  double outlineThickness = 5.0;
  Color outlineColor = Colors.black;
  Color mercuryColor = Colors.red;
  Color backgroundColor = Colors.transparent;
  bool useCustomScale = false;
  double scaleInterval = 10;
  bool mirrorScale = false;
  TempLabels labelType = TempLabels.CELCIUS;
  bool setpointEnabled = false;
  Setpoint setpoint =
      Setpoint(60, size: 9, color: Colors.blue, side: SetpointSide.both);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final tabController =
        TabController(length: 5, initialIndex: 0, vsync: this);

    return Scaffold(
        appBar: AppBar(
          title: Text('Thermometer Demo'),
        ),
        body: fullscreen
            ? Center(child: _buildDUT())
            : Column(
                children: <Widget>[
                  Container(
                    height: 300,
                    child: Center(
                      child: _buildDUT(),
                    ),
                  ),
                  Container(
                    child: _buildSettings(tabController),
                  )
                ],
              ));
  }

  Widget _buildDUT() => GestureDetector(
        child: SizedBox(
          width: thermoWidth,
          height: thermoHeight,
          child: Thermometer(
              value: value,
              minValue: minValue,
              maxValue: maxValue,
              radius: radius,
              barWidth: barWidth,
              outlineThickness: outlineThickness,
              outlineColor: outlineColor,
              mercuryColor: mercuryColor,
              backgroundColor: backgroundColor,
              scale: useCustomScale
                  ? CustomScaleProvider()
                  : IntervalScaleProvider(scaleInterval),
              mirrorScale: mirrorScale,
              label: labelType == TempLabels.CELCIUS
                  ? ThermometerLabel.celsius()
                  : labelType == TempLabels.FARENHEIT
                      ? ThermometerLabel.farenheit()
                      : ThermometerLabel('Custom',
                          textStyle: TextStyle(
                              color: Colors.blue,
                              fontSize: 16,
                              fontStyle: FontStyle.italic)),
              setpoint: setpointEnabled ? setpoint : null),
        ),
        onTap: () {
          setState(() {
            fullscreen = !fullscreen;
          });
        },
      );

  Widget _buildSettings(TabController tabController) => Column(
        children: <Widget>[
          Container(
            child: TabBar(
              labelColor: Colors.black,
              controller: tabController,
              tabs: <Widget>[
                Tab(
                  child: Text('Value'),
                ),
                Tab(text: 'Colors'),
                Tab(text: 'Geometry'),
                Tab(text: 'Scale'),
                Tab(text: 'Setpoint')
              ],
            ),
          ),
          Container(
            height: 250,
            child: TabBarView(
              controller: tabController,
              children: <Widget>[
                _buildValueTab(),
                _buildColorTab(),
                _buildGeometryTab(),
                _buildScaleTab(),
                _buildSetpointTab()
              ],
            ),
          )
        ],
      );

  Widget _buildValueTab() {
    return Table(
      columnWidths: {0: FixedColumnWidth(100)},
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: [
        TableRow(children: [
          Text('Min Value'),
          Slider(
              min: -100,
              max: 100,
              value: minValue,
              onChanged: (sliderValue) {
                setState(() {
                  minValue = min(sliderValue, maxValue - 10);
                  value = max(value, minValue);
                  setpoint.apply(value: max(setpoint.value, minValue));
                });
              })
        ]),
        TableRow(children: [
          Text('Max Value'),
          Slider(
            min: -100,
            max: 100,
            value: maxValue,
            onChanged: (sliderValue) {
              setState(() {
                maxValue = max(sliderValue, minValue + 10);
                value = min(value, maxValue);
                setpoint.apply(value: min(setpoint.value, maxValue));
              });
            },
          )
        ]),
        TableRow(children: [
          Text('Value'),
          Slider(
            min: -100,
            max: 100,
            value: value,
            onChanged: (sliderValue) {
              setState(() {
                value = max(min(sliderValue, maxValue), minValue);
              });
            },
          )
        ])
      ],
    );
  }

  Widget _buildColorTab() {
    return Table(
      columnWidths: {0: FixedColumnWidth(100)},
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: [
        TableRow(children: [
          Text('Outline'),
          CircleColor(
            color: outlineColor,
            circleSize: 35.0,
            onColorChoose: (color) {
              colorPickerDialog(context, outlineColor).then((newColor) {
                if (newColor == null) return;
                setState(() => outlineColor = newColor);
              });
            },
          )
        ]),
        TableRow(children: [
          Text('Mercury'),
          CircleColor(
              circleSize: 35.0,
              color: mercuryColor,
              onColorChoose: (color) {
                colorPickerDialog(context, outlineColor).then((newColor) {
                  if (newColor == null) return;
                  setState(() => outlineColor = newColor);
                });
              })
        ]),
        TableRow(children: [
          Text('Backgound'),
          CircleColor(
              circleSize: 35.0,
              color: backgroundColor,
              onColorChoose: (color) {
                colorPickerDialog(context, outlineColor).then((newColor) {
                  if (newColor == null) return;
                  setState(() => outlineColor = newColor);
                });
              })
        ])
      ],
    );
  }

  Widget _buildGeometryTab() {
    return Table(
      columnWidths: {0: FixedColumnWidth(100)},
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: [
        TableRow(children: [
          Text('Height'),
          Slider(
              min: 50,
              max: 300,
              value: thermoHeight,
              onChanged: (sliderValue) {
                setState(() {
                  thermoHeight = sliderValue;
                });
              })
        ]),
        TableRow(children: [
          Text('Width'),
          Slider(
            min: 50,
            max: 300,
            value: thermoWidth,
            onChanged: (sliderValue) {
              setState(() {
                thermoWidth = sliderValue;
              });
            },
          )
        ]),
        TableRow(children: [
          Text('Thickness'),
          Slider(
            min: 1,
            max: 10,
            value: outlineThickness,
            onChanged: (sliderValue) {
              setState(() {
                outlineThickness = sliderValue;
              });
            },
          )
        ]),
        TableRow(children: [
          Text('Bar Width'),
          Slider(
            min: 10,
            max: 40,
            value: barWidth,
            onChanged: (sliderValue) {
              setState(() {
                barWidth = sliderValue;
                radius = max(radius, barWidth);
              });
            },
          )
        ]),
        TableRow(children: [
          Text('Radius'),
          Slider(
            min: 10,
            max: 40,
            value: radius,
            onChanged: (sliderValue) {
              setState(() {
                radius = sliderValue;
                barWidth = min(radius, barWidth);
              });
            },
          )
        ])
      ],
    );
  }

  Widget _buildScaleTab() {
    return Table(
        columnWidths: {0: FixedColumnWidth(70)},
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: [
          TableRow(children: [
            Text('Scale Type'),
            Row(
              children: <Widget>[
                Row(children: <Widget>[
                  Radio(
                      value: true,
                      groupValue: useCustomScale,
                      onChanged: (v) {
                        setState(() {
                          useCustomScale = true;
                        });
                      }),
                  Text('Custom')
                ]),
                Row(
                  children: <Widget>[
                    Radio(
                        value: false,
                        groupValue: useCustomScale,
                        onChanged: (v) {
                          setState(() {
                            useCustomScale = false;
                          });
                        }),
                    Text('Interval')
                  ],
                )
              ],
            )
          ]),
          TableRow(children: [
            Text('Interval'),
            Slider(
              min: 5,
              max: 20,
              value: scaleInterval,
              onChanged: useCustomScale
                  ? null
                  : (sliderValue) {
                      setState(() {
                        scaleInterval = sliderValue;
                      });
                    },
            )
          ]),
          TableRow(children: [
            Text('Mirror'),
            Checkbox(
              value: mirrorScale,
              onChanged: (v) {
                setState(() {
                  mirrorScale = v ?? false;
                });
              },
            )
          ]),
          TableRow(children: [
            Text('Label'),
            Row(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Radio<TempLabels>(
                      value: TempLabels.CELCIUS,
                      groupValue: labelType,
                      onChanged: (v) => setState(() => labelType = v!),
                    ),
                    Text('Celsius')
                  ],
                ),
                Row(
                  children: <Widget>[
                    Radio<TempLabels>(
                      value: TempLabels.FARENHEIT,
                      groupValue: labelType,
                      onChanged: (v) => setState(() => labelType = v!),
                    ),
                    Text('Farenheit')
                  ],
                ),
                Row(
                  children: <Widget>[
                    Radio<TempLabels>(
                      value: TempLabels.CUSTOM,
                      groupValue: labelType,
                      onChanged: (v) => setState(() => labelType = v!),
                    ),
                    Text('Custom')
                  ],
                )
              ],
            )
          ])
        ]);
  }

  Widget _buildSetpointTab() {
    return Table(
        columnWidths: {0: FixedColumnWidth(70)},
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: [
          TableRow(children: [
            Text('Enabled'),
            Checkbox(
              value: setpointEnabled,
              onChanged: (v) {
                setState(() {
                  setpointEnabled = true;
                });
              },
            )
          ]),
          TableRow(children: [
            Text('Value'),
            Slider(
              min: minValue,
              max: maxValue,
              value: setpoint.value,
              onChanged: (sliderValue) =>
                  setState(() => setpoint = setpoint.apply(value: sliderValue)),
            )
          ]),
          TableRow(children: [
            Text('Size'),
            Slider(
              min: 5,
              max: 20,
              value: setpoint.size!,
              onChanged: useCustomScale
                  ? null
                  : (sliderValue) => setState(
                      () => setpoint = setpoint.apply(size: sliderValue)),
            )
          ]),
          TableRow(children: [
            Text('Color'),
            CircleColor(
                circleSize: 35.0,
                color: setpoint.color!,
                onColorChoose: (color) =>
                    colorPickerDialog(context, setpoint.color!).then(
                      (newColor) => setState(
                          () => setpoint = setpoint.apply(color: newColor)),
                    ))
          ]),
          TableRow(children: [
            Text('Side'),
            Row(
              children: <Widget>[
                RadioListTile<SetpointSide>(
                  value: SetpointSide.left,
                  groupValue: setpoint.side,
                  onChanged: (v) =>
                      setState(() => setpoint = setpoint.apply(side: v)),
                  title: Text('Left'),
                ),
                RadioListTile<SetpointSide>(
                  value: SetpointSide.right,
                  groupValue: setpoint.side,
                  onChanged: (v) =>
                      setState(() => setpoint = setpoint.apply(side: v)),
                  title: Text('Right'),
                ),
                RadioListTile<SetpointSide>(
                  value: SetpointSide.both,
                  groupValue: setpoint.side,
                  onChanged: (v) =>
                      setState(() => setpoint = setpoint.apply(side: v)),
                  title: Text('Both'),
                ),
              ],
            )
          ])
        ]);
  }
}

Future<Color?> colorPickerDialog(BuildContext context, Color oldColor) {
  Color selectedColor = oldColor;

  return showDialog<Color>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: const Text('Pick a color'),
            contentPadding: EdgeInsets.all(6.0),
            content: MaterialColorPicker(
              allowShades: false,
              selectedColor: selectedColor,
              onMainColorChange: (c) {
                if (c == null) return;
                selectedColor = c;
              },
            ),
            actions: <Widget>[
              TextButton(
                child: Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  Navigator.of(context).pop(selectedColor);
                },
              )
            ],
          ));
}
