# flutter_thermometer

A flutter Thermometer widget.

<p align="center">
  ![Screenshot1](./images/screenshot1.gif) ![Screenshot2](./images/screenshot2.gif) ![Screenshot3](./images/screenshot3.gif)
</p>

## Installation

Add a dependency to `pubspec.yaml`:

```
dependencies:
  flutter_thermometer: ^1.0.0
```

## Getting Started

For basic usage, three parameters are required: `minValue`, `maxValue` and `value`. The widget
must be instantiated within a fully constrained size container. In the follwing example, a
SizedBox is used:

```
SizedBox(
  width: thermoWidth,
  height: thermoHeight,
  child: Thermometer(
    value: value,
    minValue: minValue,
    maxValue: maxValue
  )
)
```

## Anatomy of the thermometer widget
<br/>
<p align="center">
  ![Anatomy](./images/anatomy.gif)
</p>
<br/>
The thermometer widget is made of several elements:
- An outline, which is characterized by `thickness`, `radius`, `barWidth` and `outlineColor` parameters.
- A Mercury bar which whose height designates the temperature. Its color is controlled bt the
  `mercuryColor` parameter.
- An optional scale.
- An optional label.
- An optional setpoint.

## Scale
The thermometer scale comprises a set of ticks, usually shown on the left side of the thermometer, but
can be also set to be shown on the right side. The scale is provided by a class implementing the
`ScaleProvider` interface. Given a minimum and a maximum value, a `ScaleProvider` computes a list of
ticks that are drawn on the widget. Each tick can have specific color, length and label. The package
comes with a single implementaion - `IntervalScaleProvider`. This implementation places ticks at constant
interval, starting from the minimum temperature. All the ticks have the same color, length and a temperature
value as a text label. 

Custom implementation can be used to show other kind of scales, for example one that has minor and major ticks.

## Label
The label is shown on the upper side of the thermometer widget, on the side opposite to the scale. It is usually
used to denote the unit of measurement shown by the thermometer. A label is created by providing an
instancel of `ThermometerLabel` to the widget. For convenience, two labels are provided by the package - degrees
Celsius and degrees Farenheit, but any other custom label can be created and styled.

## Setpoint
A setpoint is a small triangle (or two triangles) denoting a target temperature. A setpoint is created by
providing a `Setpoint` instance to the widget. The setpoint can be configured for value (temperature),
color, size and side - either on the left side, the right side or both.

# LICENSE
The project is licensed under the MIT license. See the [LICENSE](LICENSE) file for details. 
