import 'package:flutter/painting.dart';

enum SetpointSide { left, right, both }

/// Temperature setpoint.
///
/// This class represents a setpoint mark, which is displayed
/// as two triangles on both side of the termometer bar.
///
class Setpoint {
  /// The value (temperature) of the setpoint.
  final double value;

  /// The size of the setpoint mark.
  final double? size;

  /// The color of the setpoint mark.
  final Color? color;

  /// The side on which the setpoint mark is drawn.
  final SetpointSide? side;

  Setpoint(this.value,
      {required this.size, required this.color, required this.side});

  /// Creates a new copy of the object, altering the specified properties.
  Setpoint apply(
          {double? value, double? size, Color? color, SetpointSide? side}) =>
      new Setpoint(value ?? this.value,
          size: size ?? this.size,
          color: color ?? this.color,
          side: side ?? this.side);
}
